package gameA.game;

import java.util.ArrayList;

import engine.Config;
import engine.Controlable;
import engine.component.Camera;
import engine.core.Input;
import engine.gui.Hud;
import engine.gui.HudPro;
import engine.rendering.RenderingEngine;
import gameA.Block;
import gameA.game.level.Level;
import glib.util.vector.GVector2f;
import glib.util.vector.GVector3f;
import static org.lwjgl.opengl.GL11.*;
import utils.GLog;

public class Game implements GameAble{
	Controlable parent;
	private Level level;
	private ArrayList<Hud> huds = new ArrayList<Hud>();
	private Block block = new Block(new GVector2f(100), new GVector2f(200));
	HudPro hud;
	
	public Game(Controlable parent){
		this.parent = parent;
		getRenderingEngine().setMainCamera(new Camera(this, new GVector3f()));
		
		huds.add(new HudPro(getCanvasSize(), "icon_shovel", "png", new GVector2f(), getCanvasSize().div(2)));
		huds.add(new HudPro(getCanvasSize(), "block_stone", "png", new GVector2f().add(getCanvasSize().div(2)), getCanvasSize().div(2)));
		level = new Level(this);
		GLog.write(GLog.INIT, "Inicializácia objektu GameRenderingEngine bola dokončená");
	}
	
	@Override
	public void render(RenderingEngine renderingEngine) {
		glColor3f(0, 0, 1);
//		huds.stream().forEach(a -> a.render(renderingEngine));
//		level.render(renderingEngine);
		glBegin(GL_POLYGON);
		{
			block.getPoints().stream().forEach(a -> glVertex2f(a.getX(), a.getY()));
			
		}
		glEnd();

		glColor3f(0, 1, 0);
		
		block.drawShadows(Input.getMousePosition(), getCanvasSize());
//		glBegin(GL_POLYGON);
//		{
//			block.calcShadow(Input.getMousePosition(), getCanvasSize()).stream().forEach(b -> {
//				glVertex2f(b.getX(), b.getY());
//			});
//		}
//		glEnd();
		
	}
	
	@Override
	public void input() {
		if(Input.getKeyDown(Input.KEY_I))
			Config.MOUSE_ATTENUATION.addToX(0.1f);
		if(Input.getKeyDown(Input.KEY_J))
			Config.MOUSE_ATTENUATION.addToX(-0.1f);
		
		if(Input.getKeyDown(Input.KEY_O))
			Config.MOUSE_ATTENUATION.addToY(1);
		if(Input.getKeyDown(Input.KEY_K))
			Config.MOUSE_ATTENUATION.addToY(-1);
		
		if(Input.getKeyDown(Input.KEY_P))
			Config.MOUSE_ATTENUATION.addToY(10);
		if(Input.getKeyDown(Input.KEY_L))
			Config.MOUSE_ATTENUATION.addToY(-10);
	}
	
	@Override
	public void cleanUp() {
	}

	@Override
	public RenderingEngine getRenderingEngine() {
		return parent.getRenderingEngine();
	}

	@Override
	public GVector2f getCanvasSize() {
		return parent.getWindow().getCanvasSize();
	}

	@Override
	public Level getLevel() {
		return level;
	}
}
