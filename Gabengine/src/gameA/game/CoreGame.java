package gameA.game;


import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

import engine.Controlable;
import engine.core.CoreEngine;
import engine.core.Input;
import engine.gui.Menu;
import engine.rendering.RenderingEngine;
import gameA.rendering.GameRenderingEngine;
import utils.GLog;

public class CoreGame extends CoreEngine implements Controlable{
	public static final int	MAIN_MENU	= 0;
	public static final int	LOADING		= 1;
	public static final int	RUNNING		= 2;
	private GameAble game;
	private boolean exit;
	private Menu mainMenu = new MainMenu(this);
	private int gameIs = MAIN_MENU;
	private RenderingEngine renderingEngine = new GameRenderingEngine(this);
	
	public CoreGame() {
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0, Display.getWidth(), Display.getHeight(), 0, 1, -1);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		
		renderingEngine.init();
		
		GLog.write(GLog.INIT, "Inicializácia objektu CoreGame bola dokončená");
	}
	
	public void newGame(){
		gameIs = LOADING;
		initGame();
	}
	
	public void initGame(){
		game = new Game(this);
		gameIs = RUNNING;
	}
	
	@Override
	public void input() {
		if(exit)
			return;
		
		if(gameIs == RUNNING && game != null)
			game.input();
//		if(Input.getKeyDown(Input.KEY_LCONTROL))
//			initGame();
		
		if(Input.getKeyDown(Input.KEY_ESCAPE))
			exit();
	}
	
	@Override
	public void exit(){
		exit = true;
		onExit();
	}
	
	@Override
	public void render(RenderingEngine renderingEngine) {
		if(exit)
			return;
		
		this.renderingEngine.prepare();
		if(gameIs == RUNNING && game != null)
			game.render(this.renderingEngine);

		if(gameIs == MAIN_MENU)
			mainMenu.render(this.renderingEngine);

	}
	
	@Override
	public void update(float delta) {
		if(exit)
			return;
		if(gameIs == MAIN_MENU)
			mainMenu.update(delta);;
	}
	
	@Override
	public void onResize() {
		GL11.glViewport(0, 0, Display.getWidth(), Display.getHeight());
	}
	public void onExit() {
		cleanUp();
		System.exit(1);
	}
	
	@Override
	public void cleanUp() {
		if(game != null)
			game.cleanUp();
		stop();
		renderingEngine.cleanUp();
		getWindow().cleanUp();
	}

	@Override
	public RenderingEngine getRenderingEngine() {
		return renderingEngine;
	}

	@Override
	public GameAble getGame() {
		return game;
	}
}
