package gameA.game;

import java.util.HashMap;

import engine.Controlable;
import engine.core.Input;
import engine.gui.Button;
import engine.gui.GuiComponent;
import engine.gui.Menu;
import engine.rendering.RenderingEngine;
import glib.util.vector.GVector2f;
import utils.GLog;
import utils.Utils;

public class MainMenu extends Menu{
	private HashMap<String, Button> buttons = new HashMap<String, Button>();
	private Controlable parent;
	
	public MainMenu(Controlable parent){
		super(new GuiComponent(new GVector2f(), parent.getWindow().getCanvasSize()));
		this.parent = parent;
		buttons.put("newGame", new Button(parent, this, "Nová hra"));
		buttons.put("exit", new Button(parent, this, "Koniec"));
		
		GLog.write(GLog.INIT, "Inicializácia objektu MainMenu bola dokončená");
	}
	
	@Override
	public void render(RenderingEngine renderingEngine){
		buttons.entrySet().stream().map(a -> a.getValue()).forEach(a -> a.render(renderingEngine));
	}
	
	@Override
		public void update(float delta) {
			buttons.entrySet().stream().map(a -> a.getValue()).forEach(a -> a.update(delta));
			
			if(Input.isButtonDown(0)){
				if(buttons.get("newGame").isClickIn(Utils.invertHorizontali(Input.getMousePosition(), getSize())))
					parent.newGame();
				
				if(buttons.get("exit").isClickIn(Utils.invertHorizontali(Input.getMousePosition(), getSize())))
					parent.exit();
			}
		}
	
}
