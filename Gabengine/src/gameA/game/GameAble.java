package gameA.game;

import engine.core.Interactable;
import engine.rendering.RenderingEngine;
import gameA.game.level.Level;
import glib.util.vector.GVector2f;

public interface GameAble extends Interactable{
	public RenderingEngine getRenderingEngine();
	public GVector2f getCanvasSize();
	public void cleanUp();
	public Level getLevel();
}
