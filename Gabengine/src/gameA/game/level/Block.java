package gameA.game.level;

import java.awt.Color;
import java.util.HashMap;

import org.json.JSONObject;

import engine.Config;
import engine.component.Entity;
import engine.gui.HudPro;
import engine.rendering.RenderingEngine;
import gameA.game.GameAble;
import glib.util.vector.GVector2f;
import glib.util.vector.GVector3f;


public class Block extends Entity{
	public final static GVector2f SIZE  = Config.BLOCK_DEFAULT_SIZE;
	public final static String GRASS 	= "GRASS";
	public final static String WOOD 	= "WOOD";
	public final static String IRON 	= "IRON";
	public final static String FORREST  = "FORREST";
	public final static String WATER 	= "WATER";
	public final static String TILE 	= "TILE";
	public final static String ROCK 	= "ROCK";
	public final static String FUTURE 	= "FUTURE";
	public final static String LAVA 	= "LAVA";
	public final static String PATH 	= "PATH";
	public final static String STONE 	= "STONE";
	public final static String NOTHING = "NOTHING";
	
	private static HashMap<String, BlockType> blocks = new HashMap<String, BlockType>();
	
	static{
		blocks.put(NOTHING, new BlockType("block_floor.png", 0));
		blocks.put(GRASS, 	new BlockType("block_grass.png", 0));
		blocks.put(WOOD, 	new BlockType("block_wood.png", 1));
		blocks.put(IRON, 	new BlockType("block_iron.png", 10));
		blocks.put(WATER, 	new BlockType("block_water.png", 0));
//		blocks.put(ROCK, 	new BlockType("block_stone.png", 5));
		blocks.put(PATH, 	new BlockType("block_path.png", 0));
		blocks.put(STONE, 	new BlockType("block_stone.png", 7));
		blocks.put(FUTURE, 	new BlockType("block_future.png", 0));
	}
	
	private String type;	
	private int healt;
	private HudPro hud;
	
	//CONTRUCTORS
	
	public Block(JSONObject object, GameAble parent){
		this(new GVector2f(object.getString("position")),
			 object.getString("type"),
			 parent,
			 object.getInt("healt"));
	}
	
	public Block(GVector2f position, int type, GameAble parent){
		this(position, getTypeFromInt(type), parent, blocks.get(getTypeFromInt(type)).getHealt());
	}
	
	public Block(GVector2f position, String type, GameAble parent, int healt) {
		super(parent, new GVector3f(position, 1));
		this.healt = healt;
		this.type = type;
		if(type == WOOD)
			hud = new HudPro(getParent().getCanvasSize(), "block_stone", "png", getPosition().getXY().mul(Block.SIZE), Block.SIZE);
		else if(type == IRON)
			hud = new HudPro(getParent().getCanvasSize(), "block_wood", "png", getPosition().getXY().mul(Block.SIZE), Block.SIZE);
		else
			hud = new HudPro(getParent().getCanvasSize(), "block_floor", "png", getPosition().getXY().mul(Block.SIZE), Block.SIZE);
	}
	
	//OVERRIDES
	
	@Override
	public void render(RenderingEngine renderingEngine) {
		renderingEngine.renderHudPro(hud);
	}

	
	//OTHERS
	
	public static String getTypeFromInt(int num){
		switch(num){
			case 1 :
				return WOOD;
			case 2 :
				return IRON;
//			case 3 :
//				return GRASS;
//			case 4 :
//				return WATER;
//			case 5 :
//				return FUTURE;
//			case 6 :
//				return PATH;
//			case 7 :
//				return STONE;
			default :
				return NOTHING;
		}
	}
	
	public boolean hit(int demage){
		healt -= demage;
		boolean res = healt <= 0;
		if(res)
			remove();
		return res;
	}

	public void remove() {
		type = NOTHING; 
		healt = 0;
	}
	
	public void build(String type) {
		this.type = type;
		this.healt = blocks.get(type).getHealt();
	}

	//GETTERS
	
	public String getType() {return type;}
	public boolean isWalkable() {return type.equals(WATER) 	 || 
										type.equals(PATH) 	 || 
										type.equals(NOTHING) || 
										type.equals(LAVA)	 || 
										type.equals(FUTURE)	 || 
										type.equals(TILE)	 || 
										type.equals(GRASS);}
	public int getHealt() {return healt;}



	public static Color getColorbyType(String type){
		return blocks.get(type).getMinimapColor();
	}

}
	
	
