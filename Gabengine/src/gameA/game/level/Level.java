package gameA.game.level;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

import engine.core.Interactable;
import engine.rendering.RenderingEngine;
import gameA.game.GameAble;
import glib.util.vector.GVector2f;

public class Level implements Interactable {
	private final static String FROZEN = "levelFrozen";
	private Map map;
	private GameAble parent;
	private List<GVector2f> respawnZones = new ArrayList<GVector2f>();
	private String mapData;
	private JSONObject playerInfo;
	private HashMap<String, String > data;
	
	//CONSTRUCTORS
	
	
	
//	public Level(JSONObject object){
//		mapData = object.getString("map");
////		map = new Map(new JSONObject(), parent);
//		playerInfo = object.getJSONObject("playerInfo");
//		int i = 0;
//		while(object.has("respawnZone" + i)){
//			respawnZones.add(new GVector2f(object.getString(("respawnZone" + i))));
//			i++;
//		}
//		GLog.write(GLog.INIT, "Inicializácia objektu Level bola dokončená");
//	}
	
	public Level(GameAble parent){
		map = new Map(parent);
	}
	
	
	private void createRespawnZones() {
		int i=0;
		while(data.get("respawnZones" + i) != null){
			respawnZones.add(new GVector2f(data.get("respawnZones" + i++)));
		}
	}

	//OTHERS
	
	public void changeBlock(GVector2f position, int healt, int type){
		
	}

	//OVERRIDES
	
	@Override
	public void render(RenderingEngine renderingEngine) {
		map.render(renderingEngine);
	}

	
	//GETTERS
	
	public Map getMap(){return map;}
	public GameAble getParent() {return parent;}
	public boolean isReady(){return parent != null && map != null;}
	public List<GVector2f> getRespawnZones() {return new ArrayList<GVector2f>(respawnZones);}
	public GVector2f getPlayerRespawnZone(){return new GVector2f(respawnZones.get((int)(Math.random() * respawnZones.size())));}
	
	//SETTERS
	
	public void setDefaultPlayerInfo(){
		playerInfo = new JSONObject();
		playerInfo.put("speed", data.get("playerSpeed"));
		playerInfo.put("range", data.get("playerRange"));
		playerInfo.put("healt", data.get("playerHealt"));
	}
	
	public void setGame(GameAble game){
		parent = game;
		
		if(mapData != null)
			map = new Map(new JSONObject(mapData), parent);
		else
			map = new Map(game, new GVector2f(data.get("numberOfBlocks")));
	}

	public JSONObject getDefaultPlayerInfo() {
		return playerInfo;
	}


	
}
