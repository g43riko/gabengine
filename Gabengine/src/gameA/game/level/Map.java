package gameA.game.level;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONObject;

import engine.Config;
import engine.core.Interactable;
import engine.rendering.RenderingEngine;
import gameA.game.GameAble;
import glib.util.vector.GVector2f;
import utils.GLog;

public class Map implements Interactable{
	private HashMap<String, Block> blocks;
	private GVector2f numberOfBlocks;
	private GameAble parent;
	private boolean render = true;
	private long renderedBlocks;
	private String defaultMap;
	private GVector2f size;
//	private Image background; 
	
	//CONSTRUCTORS
	
	public Map(JSONObject object, GameAble parent){
		this.parent = parent;
		this.numberOfBlocks = new GVector2f(object.getString("numberOfBlocks"));
		loadMap(object);
		size = numberOfBlocks.mul(Block.SIZE);
		
//		background = createBackground(Block.getImageFromType(Block.NOTHING), numberOfBlocks, Block.SIZE);

		GLog.write(GLog.INIT, "Inicializácia objektu Map bola dokončená");
	};
	
	public Map(GameAble parent){
		this(parent, Config.MAP_DEFAULT_BLOCKS); //300 x 300 - je max
	}
	
	public Map(GameAble parent, GVector2f numberOfBlocks){
		this.parent = parent;
		this.numberOfBlocks = numberOfBlocks;
		
		createRandomMap();
		size = numberOfBlocks.mul(Block.SIZE);
//		background = createBackground(Block.getImageFromType(Block.NOTHING), numberOfBlocks, Block.SIZE);

		GLog.write(GLog.INIT, "Inicializácia objektu Map bola dokončená");
	}
	
	//OVERRIDES

	@Override
	public void render(RenderingEngine renderingEngine) {
		if(!render)
			return;
		
		blocks.entrySet().stream().map(a -> a.getValue()).forEach(a -> a.render(renderingEngine));
	}

	
	//OTHERS

	private Image createBackground(Image bgimage, GVector2f blocks, GVector2f size){
		BufferedImage background = new BufferedImage(size.getXi() * blocks.getXi(), 
													 size.getYi() * blocks.getYi(), 
													 BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = (Graphics2D) background.getGraphics();
		for(int i=0 ; i<blocks.getXi() ; i++){
			for(int j=0 ; j<blocks.getYi() ; j++){
//				g2.drawImage(bgimage, size.getXi() * i, size.getYi() * j, size.getXi(), size.getYi(), null);
				
				g2.drawImage(bgimage, 
						 bgimage.getWidth(null) * i, 
						 bgimage.getHeight(null) * j, 
						 bgimage.getWidth(null), 
						 bgimage.getHeight(null), 
						 null);
			}
		}
		g2.dispose();
		return background;
	}
	
	public void createRandomMap(){
		render = false;
		blocks = new HashMap<String, Block>();
		
		for(int i=0 ; i<numberOfBlocks.getXi() ; i++){
			for(int j=0 ; j<numberOfBlocks.getYi() ; j++){
				addBlock(i, j, new Block(new GVector2f(i,j),(int)(Math.random() * 10), parent));
			}
		}
//		clearRespawnZones(parent.getLevel().getRespawnZones());
		render = true;
	}
	
	public void loadMap(JSONObject object){
		render = false;
		blocks = new HashMap<String, Block>();
		for(int i=0 ; i<numberOfBlocks.getXi() ; i++)
			for(int j=0 ; j<numberOfBlocks.getYi() ; j++)
				addBlock(i, j, new Block(new JSONObject(object.getString("b" + i + "_" + j)), parent));
		render = true;
	}
	
	private void clearRespawnZones(List<GVector2f> zones){
		zones.stream().forEach(a -> {
			remove(a.div(Block.SIZE).toInt());
		});
	}
	
	public void remove(GVector2f sur){
		Block b = getBlock(sur.getXi(), sur.getYi());
		if(b != null && b.getType() != Block.NOTHING)
			b.remove();
	}
	
	public void resetMap() {
		loadMap(new JSONObject(defaultMap));
	}
	
	//ADDERS

	private void addBlock(int i, int j, Block block){
		blocks.put(i + "_" + j, block);
	}
	
	//GETTERS
	
	public long getRenderedBlocks() {return renderedBlocks;}
	public GVector2f getNumberOfBlocks() {return numberOfBlocks;}
	public Block getBlock(String block){return blocks.get(block);}	
	public Block getBlock(int i, int j){return blocks.get(i + "_" + j);}
	public GameAble getParent() {return parent;}
	
	public boolean isWalkable(int i, int j){
		Block b = getBlock(i, j);
		return b != null && b.isWalkable();
	}

	public int[] getPossibleWays(GVector2f sur){
		ArrayList<Integer> result = new ArrayList<Integer>();
		Block b;
		
		b = getBlock(sur.getXi(), sur.getYi() - 1);
		if(b != null && b.isWalkable())
			result.add(0);
		
		b = getBlock(sur.getXi() + 1, sur.getYi());
		if(b != null && b.isWalkable())
			result.add(1);
		
		b = getBlock(sur.getXi(), sur.getYi() + 1);
		if(b != null && b.isWalkable())
			result.add(2);
		
		b = getBlock(sur.getXi() - 1, sur.getYi());
		if(b != null && b.isWalkable())
			result.add(3);
		
		int[] ret = new int[result.size()];
		for(int i=0 ; i<result.size() ; i++)
			ret[i] = result.get(i);
		
		return ret;
	}
	
	public Block getRandomEmptyBlock(){
		ArrayList<Block> b = blocks.entrySet()
								   .stream()
								   .map(a -> a.getValue())
								   .filter(a -> a.getType() == Block.NOTHING)
								   .collect(Collectors.toCollection(ArrayList<Block>::new));
		return b.get((int)(Math.random() * b.size()));
	}
	
	public Block getBlockOnPosition(GVector2f sur){
		GVector2f blockSize = Block.SIZE;
		GVector2f pos = sur.sub(sur.mod(blockSize)).div(blockSize);
		
		return getBlock(pos.getXi(), pos.getYi());
	}

	public ArrayList<Block> getBlocks(){
		if(render)
			return new ArrayList<Block>(blocks.values());
		
		return new ArrayList<Block>();
	}
	
	public GVector2f getSize(){
		return size;
	}

}
