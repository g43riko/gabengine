package gameA;

import java.util.ArrayList;

import glib.util.vector.GVector2f;
import static org.lwjgl.opengl.GL11.*;
import glib.util.vector.GVector3f;

public class Block {
	private GVector2f position;
	private GVector2f size;

	GVector3f min = null;
	GVector3f max = null; 
	GVector3f shortest = null;
	
	public Block(GVector2f position, GVector2f size) {
		this.position = position;
		this.size = size;
	}
	
	public ArrayList<GVector2f> calcShadow(GVector2f point, GVector2f mapSize){
		min = max = shortest = null;
		point.setY(mapSize.getY() - point.getY());
		ArrayList<GVector2f> list = getPoints();
		list.stream().forEach(a -> {
			float angle = (float)Math.atan2(point.getY() - a.getY(), point.getX() - a.getX());
			float dist  = a.dist(point);
			if(shortest == null)
				shortest = new GVector3f(a, dist);
			else
				if(shortest.getZ() > dist)
					shortest = new GVector3f(a, dist);
			
			if(min == null)
				min = new GVector3f(a, angle);
			else
				if(min.getZ() > angle)
					min = new GVector3f(a, angle);
			
			if(max == null)
				max = new GVector3f(a, angle);
			else
				if(max.getZ() < angle)
					max = new GVector3f(a, angle);
			
		});	
		
		
		list = new ArrayList<GVector2f>();
		
		GVector2f vMin = min.getXY().add(min.getXY().sub(point));
		GVector2f vMax = max.getXY().add(max.getXY().sub(point));
		
		list.add(shortest.getXY());
		list.add(min.getXY());
		list.add(new GVector2f(vMin.getX() * 2, mapSize.getY()).sub(vMin));
		list.add(new GVector2f(vMax.getX() * 2, mapSize.getY()).sub(vMax));
		list.add(max.getXY());
		
		
		return list;
	}
	
	public ArrayList<GVector2f> drawShadows(GVector2f point, GVector2f mapSize){
		ArrayList<GVector2f> list = getPoints();
		point = new GVector2f(point.getX() * 2, mapSize.getY()).sub(point);
		for(int i=0 ; i<list.size() ; i++){
			GVector2f currentVertex = list.get(i);
			GVector2f nextVertex = list.get((i + 1) % list.size());
			GVector2f edge = nextVertex.sub(currentVertex);
			GVector2f normal = new GVector2f(edge.getY(), -edge.getX());
			GVector2f lightToCurrent = currentVertex.sub(point);
			
			if(normal.dot(lightToCurrent) > 0){
				GVector2f point1 = currentVertex.add(currentVertex.sub(point).Normalized().mul(mapSize.max()));
				GVector2f point2 = nextVertex.add(nextVertex.sub(point).Normalized().mul(mapSize.max()));
				glBegin(GL_QUADS); {
					glVertex2f(currentVertex.getX(), currentVertex.getY());
					glVertex2f(point1.getX(), point1.getY());
					glVertex2f(point2.getX(), point2.getY());
					glVertex2f(nextVertex.getX(), nextVertex.getY());
				} glEnd();
			}
		}
		return list;
	}
	
	public  ArrayList<GVector2f> getPoints(){
		ArrayList<GVector2f> result = new ArrayList<GVector2f>();
		
		result.add(new GVector2f(position));
		result.add(new GVector2f(position.add(size.mul(new GVector2f(1, 0)))));
		result.add(new GVector2f(position.add(size)));
		result.add(new GVector2f(position.add(size.mul(new GVector2f(0, 1)))));
		return result;
	}
	
	
	public GVector2f getPosition() {
		return position;
	}
	public GVector2f getSize() {
		return size;
	}
}
