package gameA.rendering;

import engine.Controlable;
import engine.rendering.RenderingEngine;
import utils.GLog;

public class GameRenderingEngine extends RenderingEngine{
//	private GameAble parent;
	
	public GameRenderingEngine(Controlable parent){
		super(parent);
		GLog.write(GLog.INIT, "Inicializácia objektu GameRenderingEngine bola dokončená");
	}
}
