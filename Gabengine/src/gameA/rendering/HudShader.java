package gameA.rendering;

import engine.rendering.BasicShader;

public class HudShader extends BasicShader{

	public HudShader() {
		super("hudShader");
	}

	@Override
	protected void bindAttributes() {
		bindAttribute(0, "position");
	}

	@Override
	public void getAllUniformsLocations() {
		uniforms.put("transformationMatrix", super.getUniformLocation("transformationMatrix"));
		uniforms.put("guiTexture", super.getUniformLocation("guiTexture"));
		uniforms.put("normalSampler", super.getUniformLocation("normalSampler"));
		uniforms.put("mousePosition", super.getUniformLocation("mousePosition"));

		uniforms.put("falloff", super.getUniformLocation("falloff"));
		uniforms.put("resolution", super.getUniformLocation("resolution"));
		uniforms.put("lightColor", super.getUniformLocation("lightColor"));
		uniforms.put("ambientColor", super.getUniformLocation("ambientColor"));
		
	}
	
	public void connectTextures(){
		updateUniform("guiTexture", 0);
		updateUniform("normalSampler", 1);
	}

}
