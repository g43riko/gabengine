package utils;


import java.awt.Font;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

import engine.gui.GuiComponent;
import glib.util.vector.GVector2f;
import glib.util.vector.GVector3f;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.opengl.TextureImpl;

public class Log {
	private static TrueTypeFont font;
	private static boolean antiAlias = true;
	private static boolean show = true;
	static{
		Font awtFont = new Font("Times New Roman", Font.BOLD, 24);
		font = new TrueTypeFont(awtFont, antiAlias);
		
	}
	
	private static void init(){
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glShadeModel(GL11.GL_SMOOTH);        
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glDisable(GL11.GL_LIGHTING);                    
 
//		GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);                
//      GL11.glClearDepth(1);                                       
 
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
 
//      GL11.glViewport(0,0,800,600);
//		GL11.glMatrixMode(GL11.GL_MODELVIEW);
 
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0, Display.getWidth(), Display.getHeight(), 0, 1, -1);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
	}

	public static void toogle(){
		show = show == false;
	}
	
	public static void render(GuiComponent parent){
		TextureImpl.bindNone();
		init();
		GL20.glUseProgram(0);
		
		GVector2f pos = new GVector2f(parent.getPosition());
		TrueTypeFont f = new TrueTypeFont(new Font("Times New Roman", Font.BOLD, parent.getTextSize()), antiAlias);
		
		if(parent.getTextOffset().getX() == GuiComponent.CENTER)
			pos.addToX((parent.getSize().getX() - f.getWidth(parent.getText()) ) / 2);
		
		if(parent.getTextOffset().getY() == GuiComponent.CENTER)
			pos.addToY((parent.getSize().getY() - f.getHeight(parent.getText()) ) / 2);
		
		f.drawString(pos.getX(), 
					 pos.getY(), 
					 parent.getText(),
					 new Color(parent.getTextColor().getX() * 255, 
							   parent.getTextColor().getY() * 255, 
							   parent.getTextColor().getZ() * 255));
		
		GL11.glDisable(GL11.GL_BLEND);
	}
	
	public static void render(String text, GVector2f position, int textSize, GVector3f color){
		TextureImpl.bindNone();
		init();
		GL20.glUseProgram(0);
		new TrueTypeFont(new Font("Times New Roman", 
								  Font.BOLD, 
								  textSize), antiAlias).drawString(position.getX(), 
										  						   position.getY(), 
										  						   text,
										  						   new Color(color.getX() * 255, 
										  								     color.getY() * 255, 
										  								     color.getZ() * 255));
		GL11.glDisable(GL11.GL_BLEND);
	}
	
	public static void render(String text, GVector2f position){
		TextureImpl.bindNone();
		init();
		GL20.glUseProgram(0);
		
		Color.white.bind();
		font.drawString(position.getX(), position.getY(), text);
		GL11.glDisable(GL11.GL_BLEND);
	}
	
	public static void render(String ... texts){
		if(!show)
			return;
		
		TextureImpl.bindNone();
		init();
		GL20.glUseProgram(0);
		
		Color.white.bind();
		for(int i=0 ; i<texts.length ; i++)
			font.drawString(0, i * 24, texts[i]);
		
		GL11.glDisable(GL11.GL_BLEND);
	}
}
