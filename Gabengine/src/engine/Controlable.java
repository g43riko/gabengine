package engine;

import engine.core.Window;
import engine.rendering.RenderingEngine;
import gameA.game.GameAble;

public interface Controlable {
	public void exit();
	public void newGame();
	public void initGame();
	public RenderingEngine getRenderingEngine();
	public Window getWindow();
	public GameAble getGame();
}
