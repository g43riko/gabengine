package engine;

import glib.util.vector.GVector2f;
import glib.util.vector.GVector3f;
import static org.lwjgl.opengl.GL11.*;
import glib.util.vector.GVector4f;

public class Config {
	public final static int FPS = 60;
	
	public final static String		WINDOW_DEFAULT_TITLE		= "Gabgine";
	public final static GVector2f	WINDOW_DEFAULT_SIZE			= new GVector2f(800, 600);
	public static final boolean		WINDOW_DEFAULT_FULLSCREEN	= false;
	
	public final static int TEXTURE_DEFAULT_FILTER = /*GL_SMOOTH;*/GL_NEAREST;
	public final static int TEXTURE_DEFAULT_WRAP = GL_REPEAT;//GL_CLAMP_TO_EDGE;

	
	public static final GVector3f WINDOW_DEFAULT_BACKGROUND = new GVector3f(1, 0, 1);

	public static final GVector4f	MOUSE_LIGHT_COLOR	= new GVector4f(5, 5, 5, 1);
	public final static GVector3f	MOUSE_ATTENUATION	= new GVector3f(.8, 6, 40);
	public static final GVector4f	MOUSE_AMBIENT_COLOR	= new GVector4f(.1, .1, .1, 1);

	public static final GVector2f BLOCK_DEFAULT_SIZE = new GVector2f(20);
	
	public static final GVector2f MAP_DEFAULT_BLOCKS = new GVector2f(40, 40);
}
