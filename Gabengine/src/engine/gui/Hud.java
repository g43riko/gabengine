package engine.gui;

import engine.core.CoreEngine;
import engine.core.Model;
import engine.rendering.RenderingEngine;
import engine.rendering.Texture2D;
import glib.util.vector.GMatrix4f;
import glib.util.vector.GVector2f;
import utils.Maths;

public class Hud{
	private final static Model MODEL = CoreEngine.getLoader().loadToVAO(new float[]{-1,1,-1,-1,1,1,1,-1});
	private Texture2D texture;
	private GVector2f position;
	private GVector2f scale;
	//CONSTRUCTORS
	
	public Hud(GVector2f totalSize, Texture2D texture, GVector2f position, GVector2f scale){
		this.scale = scale.div(totalSize);
		
		this.position = position.div(totalSize.div(2));
		this.position = this.position.sub(1).add(this.scale).mul(new GVector2f(1, -1));
		this.texture = texture;
	}

	//OTHERS
	
	public void render(RenderingEngine renderingEngine) {
		renderingEngine.renderHud(this);
	}

	//GETTERS

	public GMatrix4f getTransformationMatrix() {
		return Maths.MatrixToGMatrix(Maths.createTransformationMatrix(position, scale));
	}

	public Model getModel(){
		return MODEL;
		
	}
	
	public Texture2D getTexture() {
		return texture;
	}

	//SETTERS
	
	public void setTexture(Texture2D texture) {
		this.texture = texture;
	}
	
}

