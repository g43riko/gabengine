package engine.gui;


import java.util.ArrayList;
import java.util.HashMap;

import engine.core.Input;
import engine.core.Interactable;
import engine.core.Model;
import engine.rendering.RenderingEngine;
import engine.rendering.Texture2D;
import glib.interfaces.Visible;
import glib.math.GColision;
import glib.util.vector.GVector2f;
import glib.util.vector.GVector3f;
import utils.Utils;


public class GuiComponent implements Interactable, Visible{
	public final static float CENTER = -0.123456789f;

	public static HashMap<GuiComponent, Integer>	buttons	= new HashMap<GuiComponent, Integer>();
	private ArrayList<GuiComponent>					childs	= new ArrayList<GuiComponent>();
	
	private GuiComponent 	parent;
	private GVector2f		offset				= new GVector2f();
	private GVector2f		textOffset			= new GVector2f(CENTER);
	private GVector2f		pos;
	private GVector2f		size;
	private int				textSize			= 40;
	private int				round				= 0;
	private int				borderWidth			= 0;
	protected int			topCousePrevButtons	= 0;
	private boolean			hover;
	private boolean			disable				= false;
	private boolean			value				= true;
	private String			text				= "gabrielko je super";
	private String			font				= "Garamond";
	private GVector3f		bgColor				= new GVector3f(1, 1, 0);
	private GVector3f		borderColor;
	private GVector3f		textColor			= new GVector3f(0, 1, 1);
	private GVector3f		hoverColor			= new GVector3f(0, 1, 0);
	private GVector3f		diableColor;
	private Texture2D 		texture;//				= new Texture2D("icon_shovel.png");
	private Model 			model;
	
	
	
	public GuiComponent(GVector2f position, GVector2f size) {
		parent = null;
		this.pos = position;
		this.size = size;
	}

	public GuiComponent(GuiComponent parent){
		this.parent = parent;
		pos = new GVector2f(parent.getPosition());
		size = new GVector2f(parent.getSize());
	}
	
	public void calcPosition(){
		pos.addToY(topCousePrevButtons);
		if(getPosition().getX() == GuiComponent.CENTER && parent != null)
			getPosition().setX(parent.getPosition().getX() + (parent.getSize().getX() - getSize().getX()) / 2);
		
		if(getPosition().getY() == GuiComponent.CENTER && parent != null)
			getPosition().setY(parent.getPosition().getY() + (parent.getSize().getY() - getSize().getY()) / 2);
	}
	
	@Override
	public void render(RenderingEngine renderingEngine) {
		childs.stream().forEach(a -> a.render(renderingEngine));
		
	}
	
	@Override
	public void update(float delta) {
		if(disable)
			return;
		
		childs.stream().forEach(a -> a.update(delta));
		
		hover = GColision.pointRectCollision(pos, size, Utils.invertHorizontali(Input.getMousePosition(), parent.getSize())); 
		
//		if(this instanceof Button){
//			System.out.println(hover);
//			System.out.println("pos: " + pos);
//			System.out.println("size: " + size);
//			System.out.println("Input.getMousePosition(): " + parent.getSize().sub(new GVector2f(0, Input.getMousePosition().getY())));
//		}
	}

	public ArrayList<GuiComponent> getChilds() {return childs;}
	
	public GVector2f 	getSize() {return size;}
	public GVector2f 	getPosition() {return pos;}
	public GVector2f 	getOffset() {return offset;}
	public GVector3f 	getBgColor() {return bgColor;}
	public GVector3f 	getTextColor() {return textColor;}
	public GVector2f 	getTextOffset() {return textOffset;}
	public GVector3f 	getHoverColor() {return hoverColor;}
	public GVector3f 	getBackgroundColor() {return bgColor;}
	public GVector3f 	getDiableColor() {return diableColor;}
	public String 		getText() {return text;}
	public String 		getFont() {return font;}
	public boolean 		isHover() {return hover;}
	public boolean 		isDisable() {return disable;}
	public GuiComponent getParent() {return parent;}
	public Texture2D 	getTexture() {return texture;}
	public int 			getTextSize() {return textSize;}

	public void setText(String text) {this.text = text;}
	public void setSize(GVector2f size) {this.size = size;}
	public void setPosition(GVector2f position) {this.pos = position;}
	public void setBackgroundColor(GVector3f backgroundColor) {this.bgColor = backgroundColor;}

	public void addChild(GuiComponent child){
		childs.add(child);
	}
	
	protected void clickIn(){
		value = !value;
	};
	
	public boolean isClickIn(GVector2f click){
		if(disable)
			return false;
			
		boolean result = GColision.pointRectCollision(pos, size, click);
		
		if(result){
			clickIn();
			childs.stream().forEach(a -> a.isClickIn(click));
		}
		return result; 
	}

	public void setOffset(GVector2f offset) {
		this.offset = offset;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

	
	
	
}

