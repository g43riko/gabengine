package engine.gui;

import engine.rendering.RenderingEngine;
import engine.rendering.Texture2D;
import glib.util.vector.GVector2f;

public class HudPro extends Hud{
	private Texture2D normal;
	
	public HudPro(GVector2f totalSize, String fileName, String pripona, GVector2f position, GVector2f scale) {
		super(totalSize, new Texture2D(fileName + "." + pripona), position, scale);
		normal = new Texture2D(fileName + "_normal." +  pripona);
	}
	
	public void render(RenderingEngine renderingEngine) {
		renderingEngine.renderHudPro(this);
	}

	public Texture2D getNormal() {
		return normal;
	}

}
