package engine.gui;

import engine.Controlable;
import engine.rendering.RenderingEngine;
import engine.rendering.Texture2D;
import glib.util.vector.GVector2f;
import utils.Log;
import static org.lwjgl.opengl.GL11.*;

public class Button extends GuiComponent{
	private Hud hud; 
	
	public Button(Controlable controlable, GuiComponent parent, String text){
		super(parent);
		setPosition(new GVector2f(GuiComponent.CENTER, 100));
		setSize(new GVector2f(500, 50));
		setText(text);
		setOffset(new GVector2f(0, 20));
		
		
		if(!buttons.containsKey(parent))
			buttons.put(parent, 0);

		topCousePrevButtons = buttons.get(parent);
		buttons.put(parent, topCousePrevButtons + getSize().getYi() + getOffset().getYi());
		
		calcPosition();
		hud = new Hud(controlable.getWindow().getCanvasSize(), new Texture2D("icon_shovel.png"), getPosition(), getSize());
	}
	
	@Override
	public void render(RenderingEngine renderingEngine) {
		if(getTexture() != null){
			getTexture().bind();
			glColor3f(1,1,1);
		}
		else{
			if(isHover())
				glColor3f(getHoverColor().getX(),getHoverColor().getX(),getHoverColor().getZ());
			else if(isDisable())
				glColor3f(getDiableColor().getX(),getDiableColor().getX(),getDiableColor().getZ());
			else
				glColor3f(getBgColor().getX(),getBgColor().getX(),getBgColor().getZ());
		}
		
		glBegin(GL_QUADS);
		{	
			if(getTexture() != null)
				glTexCoord2f(0,0);	
			glVertex3f(getPosition().getX(), getPosition().getY(), 1);
			
			
			if(getTexture() != null)
				glTexCoord2f(1,0);
			glVertex3f(getPosition().getX() + getSize().getX(), getPosition().getY(), 1);
			
			if(getTexture() != null)
				glTexCoord2f(1,1);
			glVertex3f(getPosition().getX() + getSize().getXi(), getPosition().getY() + getSize().getY(), 1);
			
			if(getTexture() != null)
				glTexCoord2f(0,1);
			glVertex3f(getPosition().getX(), getPosition().getY() + getSize().getY(), 1);
		}
		glEnd();
		
		
		renderingEngine.renderHud(hud);
		
		Log.render(this);
	}
}
