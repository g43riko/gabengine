package engine.component;

import engine.core.Interactable;
import gameA.game.GameAble;
import glib.util.IDGenerator;
import glib.util.vector.GVector3f;

public class Entity implements Interactable{
	private	int			id		= IDGenerator.getId();
	private GameAble	parent;
	private GVector3f	position;
	private GVector3f	rotation;
	private GVector3f	scale;
	private boolean		active	= true;
	private boolean		alive	= true;

	public Entity(GameAble parent, GVector3f position) {
		this(parent, position, new GVector3f(), new GVector3f());
	}
	
	public Entity(GameAble parent, GVector3f position, GVector3f rotation, GVector3f scale) {
		this.parent = parent;
		this.position = position;
		this.rotation = rotation;
		this.scale = scale;
	}

	public void rotate(GVector3f vec){
		rotation = rotation.add(vec);
	}
	
	public void move(GVector3f move){
		position = position.add(move);
	}
	
	public boolean 		isActive() {return active;}
	public boolean 		isAlive() {return alive;}
	public GVector3f	getPosition() {return position;}
	public GVector3f 	getRotation() {return rotation;}
	public GVector3f 	getScale() {return scale;}
	public int 			getId() {return id;}
	public GameAble 	getParent() {return parent;}
	
	public void setPosition(GVector3f position) {this.position = position;}
	public void setRotation(GVector3f rotation) {this.rotation = rotation;}
	public void setScale(GVector3f scale) {this.scale = scale;}
	public void setActive(boolean active) {this.active = active;}
	public void setAlive(boolean alive) {this.alive = alive;}
}
