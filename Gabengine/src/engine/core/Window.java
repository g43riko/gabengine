package engine.core;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;

import javax.swing.JFrame;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import engine.Config;
import glib.util.vector.GVector2f;
import utils.GLog;

public class Window extends JFrame{
	private static final long serialVersionUID = 1L;
	
	private GVector2f	size	= new GVector2f(Config.WINDOW_DEFAULT_SIZE);
	private Canvas		canvas	= new Canvas();
	
	public Window(boolean fullScreen){
		if(fullScreen){
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			size.setX((int)screenSize.getWidth());
			size.setY((int)screenSize.getHeight());
			setExtendedState(Frame.MAXIMIZED_BOTH);
			setUndecorated(true);
			
		}
		
		setSize(size.getXi(), size.getYi());
		setTitle(Config.WINDOW_DEFAULT_TITLE);
		setResizable(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		add(canvas);
		setVisible(true);
		
		initOpenGL();
		GLog.write(GLog.INIT, "Inicializácia objektu Window bola dokončená");
	}
	
	private void initOpenGL(){
		try {
			Display.setParent(canvas);
	        Display.create();
	        Keyboard.create();
			Mouse.create();
		} catch (LWJGLException e) {
			GLog.write(GLog.INIT, "Nastala chyba pri vytváraní okna vo funkcií initOpenGL()");
		}
	}
	
	public void cleanUp(){
		Keyboard.destroy();
		Mouse.destroy();
		Display.destroy();
	}

	public GVector2f getCanvasSize() {
		return new GVector2f(canvas.getWidth(), canvas.getHeight());
	}
}
