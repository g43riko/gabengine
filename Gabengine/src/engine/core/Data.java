package engine.core;

import java.util.HashMap;

public class Data {
	public final static String BOOLEAN = "boolean";
	public final static String STRING = "string";
	public final static String FLOAT = "float";
	
	private HashMap<String, String> data = new HashMap<String, String>();
	
	public String getData(String name){
		if(data.containsKey(name))
			return data.get(name);
		
		return null;
	}
	
	public void setData(String key, String value){
		if(key != null && value != null)
			data.put(key, value);
	}
	
	public String getType(String value){
		
		if(value == null)
			return null;
		
		if(value.toLowerCase() == "true" || value.toLowerCase() == "false")
			return BOOLEAN;
		
		try {
			Float.parseFloat(value);
			return FLOAT;
		} catch(Exception e){
			return STRING;
		}
	}
	
	public HashMap<String, String> getData(){
		return new HashMap<String, String>(data);
	}
	
}
