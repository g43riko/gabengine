package engine.core.analize;

import java.util.ArrayList;
import java.util.HashMap;

import utils.GLog;

public class Performance {
	private static ArrayList<FrameData> datas = new ArrayList<FrameData>(); 
	private static int fps = 0;
	private static int actFps = 0;
	private static StringBuilder data;
	
	private static ArrayList<Long>	renderTimes	= new ArrayList<Long>();
	private static ArrayList<Long>	updateTimes	= new ArrayList<Long>();
	private static ArrayList<Long>	waitTimes	= new ArrayList<Long>();
	private static ArrayList<Long>	totalTimes	= new ArrayList<Long>();
	
	private static HashMap<String, Long> times = new HashMap<String, Long>(); 
	
	public static void increaseFPS(){
		actFps++;
	}

	public static void loopEnd() {
		fps = actFps;
		actFps = 0;
		
		long averageRenderTimes = (long)renderTimes.stream().mapToLong(a -> a).average().getAsDouble();
		long averageUpdateTimes = (long)updateTimes.stream().mapToLong(a -> a).average().getAsDouble();
		long averageWaitTimes = (long)waitTimes.stream().mapToLong(a -> a).average().getAsDouble();
		long averageTotalTimes = (long)totalTimes.stream().mapToLong(a -> a).average().getAsDouble();

		renderTimes.clear();
		updateTimes.clear();
		waitTimes.clear();
		totalTimes.clear();
		
		data = new StringBuilder("render: " + averageRenderTimes);
		data.append(" update: " + averageUpdateTimes);
		data.append(" wait: " + averageWaitTimes);
		data.append(" total: " + averageTotalTimes);
		data.append(" all: " + (averageWaitTimes + averageUpdateTimes + averageRenderTimes));
		data.append(" fps:" + fps);
		
		GLog.write(GLog.PERFORMANCE, data.toString());
	}
	
	
	public static void setRenderTime(long time){renderTimes.add(time);}
	public static void setUpdateTime(long time){updateTimes.add(time);}
	public static void setWaitTime(long time){waitTimes.add(time);}
	public static void setTotalTime(long time){totalTimes.add(time);}
	public static void setTime(String name){times.put(name, System.nanoTime());}

	public static int getFPS(){return fps;}
	public static long getTime(String name){return System.nanoTime() - times.get(name);}
}
