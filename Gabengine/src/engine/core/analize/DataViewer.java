package engine.core.analize;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import engine.core.Data;

public class DataViewer extends JFrame{
	private static final long serialVersionUID = 1L;
	private DataViewer toto = this;
	private int i = 0;
	
	public DataViewer(Data data, int collums){
		setSize(800, 600);
		setLayout(new GridBagLayout());
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		GridBagConstraints c = new GridBagConstraints();

		
		data.getData().entrySet().stream().forEach(a -> {
			c.gridx = i % (collums * 2);
			c.gridy = i / (collums * 2);
			
//			toto.add(toto.getComponents(a.getKey(), a.getValue(), data), c);
			toto.add(new JLabel(a.getKey()), c);
			i++;
			c.gridx = i % (collums * 2);
			c.gridy = i / (collums * 2);
			
			switch(data.getType(a.getValue())){
				case Data.BOOLEAN :
					JCheckBox b = new JCheckBox("", Boolean.parseBoolean(a.getValue()));
					b.addActionListener(q -> data.setData(a.getKey(), String.valueOf(b.isSelected())));
					add(b, c);
					break;
				case Data.STRING :
					JTextField e = new JTextField(a.getValue());
					e.addActionListener(q -> data.setData(a.getKey(), e.getText()));
					add(e, c);
					break;
				case Data.FLOAT :
					JTextField d = new JTextField(a.getValue());
					d.addActionListener(q -> data.setData(a.getKey(), d.getText()));
					add(d, c);
					break;
			}
			
			i++;
		});
		
		setVisible(true);
	}
	
	private JPanel getComponents(String name, String value, Data data){
		JPanel panel = new JPanel();
		
		panel.setBackground(Color.red);
		panel.add(new JLabel(name));
		
		switch(data.getType(value)){
			case Data.BOOLEAN :
				JCheckBox b = new JCheckBox("", Boolean.parseBoolean(value));
				b.addActionListener(a -> data.setData(name, String.valueOf(b.isSelected())));
				panel.add(b);
				break;
			case Data.STRING :
				JTextField c = new JTextField(value);
				c.addActionListener(a -> data.setData(name, c.getText()));
				panel.add(c);
				break;
			case Data.FLOAT :
				JTextField d = new JTextField(value);
				d.addActionListener(a -> data.setData(name, d.getText()));
				panel.add(d);
				break;
		}
		panel.validate();
		return panel;
	}
}
