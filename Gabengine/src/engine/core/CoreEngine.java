package engine.core;

import org.lwjgl.opengl.Display;

import engine.Config;
import engine.core.analize.Performance;
import utils.GLog;

public abstract class CoreEngine implements Interactable{
	private static Loader	loader	= new Loader();
	private boolean			running	= false;
	private Window			window	= new Window(Config.WINDOW_DEFAULT_FULLSCREEN);
	
	public CoreEngine(){
		GLog.write(GLog.INIT, "Inicializácia objektu CoreEngine bola dokončená");
	}
	
	public void start(){
		running = true;
		
		long time = System.nanoTime();
		while(running && !Display.isCloseRequested()){
			Performance.setTime("total");
			defaultInput();
			
			Performance.setTime("update");
			defaultUpdate(1);
			Performance.setUpdateTime(Performance.getTime("update"));
			
			Performance.setTime("render");
			defaultRender();
			Performance.setRenderTime(Performance.getTime("render"));
			
			if(System.nanoTime() - time < 1000000000)
				Performance.increaseFPS();
			else{
				Performance.loopEnd();
				time = System.nanoTime();
			}
			if(!Display.isCreated())
				return;
			Display.update();
			
			Performance.setTime("wait");
			Display.sync(Config.FPS);
			Performance.setWaitTime(Performance.getTime("wait"));
			
			Performance.setTotalTime(Performance.getTime("total"));
		}
		stop();
	}
	
	public void stop(){
		running = false;
	}
	
	
	private void defaultRender() {
		render(null);
	}
	
	private void defaultUpdate(float delta) {
		if(Display.wasResized())
			onResize();
		
		update(delta);
	}
	
	private void defaultInput() {
		input();
	}
	
	public abstract void onExit();
	public abstract void onResize();

	public Window getWindow() {return window;}
	public static Loader getLoader() {return loader;}
}
