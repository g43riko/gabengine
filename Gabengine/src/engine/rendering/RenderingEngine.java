package engine.rendering;

import java.util.HashMap;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import static org.lwjgl.opengl.GL11.*;

import engine.Config;
import engine.Controlable;
import engine.component.Camera;
import engine.core.Input;
import engine.core.Model;
import engine.gui.Hud;
import engine.gui.HudPro;
import gameA.rendering.HudShader;
import utils.GLog;

public abstract class RenderingEngine {
	private HashMap<String, Boolean> variables = new HashMap<String, Boolean>();
	private Camera mainCamera;
	private Controlable parent;
	private static HashMap<String, BasicShader> shaders = new HashMap<String, BasicShader>(); 
	
	static{
		shaders.put("guiShader", new HudShader());
	}
	
	public RenderingEngine(Controlable parent){
		this.parent = parent;
		glClearColor(Config.WINDOW_DEFAULT_BACKGROUND.getX(), 
					 Config.WINDOW_DEFAULT_BACKGROUND.getY(), 
					 Config.WINDOW_DEFAULT_BACKGROUND.getZ(), 
					 1f);
		
		GLog.write(GLog.INIT, "Inicializ�cia objektu RenderingEngine bola dokon�en�");
	}
	
	public void prepare(){
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	}

	public void setMainCamera(Camera mainCamera) {
		this.mainCamera = mainCamera;
	}

	public void cleanUp() {
	}

	public void init() {
		glEnable(GL_TEXTURE_2D);
	}
	
	public void render(Model model){
		//bind cube
		
		
		System.out.println("kresl�");
		GL30.glBindVertexArray(model.getVaoID());
		GL20.glEnableVertexAttribArray(0);//pre x,y,z - pos. suradnice
		GL20.glEnableVertexAttribArray(1);//pre u,v - texturu
		GL20.glEnableVertexAttribArray(2);//x,y,z - pre norm�lu
		
//		Matrix4f transformationMatrix = Maths.createTransformationMatrix(new Vector3f(entity.getX(),entity.getY(),entity.getZ()), 
//																		 entity.getRx(), entity.getRy(), entity.getRz(), entity.getScale());
//		shader.loadTransformationMatrix(transformationMatrix);
		
//		ModelTexture texture = texturedModel.getTexture();
		
		//load shine variables
		//shader.loadShineVariables(texture.getShineDamper(), texture.getReflectivity());
		
		//bind texture
			GL13.glActiveTexture(GL13.GL_TEXTURE0);
//			GL11.glBindTexture(GL11.GL_TEXTURE_2D, texturedModel.getTexture().getID());//pripoj� text�ru
			GL11.glDrawElements(GL11.GL_TRIANGLES, model.getVertexCount(),GL11.GL_UNSIGNED_INT, 0);//vykresl� model
		
		//unbind cube	
		GL20.glDisableVertexAttribArray(0);
		GL20.glDisableVertexAttribArray(1);
		GL20.glDisableVertexAttribArray(2);
		GL30.glBindVertexArray(0);
	}
	
	public void renderHud(Model model){
//		if(!variables.containsKey("hud") || !variables.get("hud"))
//			return;
		
//		shaders .get("guiShader").bind();
		GL30.glBindVertexArray(model.getVaoID());
		GL20.glEnableVertexAttribArray(0);
		
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		//render
		
//			GL13.glActiveTexture(GL13.GL_TEXTURE0);
			
			GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, model.getVertexCount());
		
//		GL11.glEnable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL20.glDisableVertexAttribArray(0);
		GL30.glBindVertexArray(0);
	}
	
	public void renderHud(Hud hud){
//		if(!variables.containsKey("hud") || !variables.get("hud"))
//			return;
		
		
		shaders.get("guiShader").bind();
		GL30.glBindVertexArray(hud.getModel().getVaoID());
		GL20.glEnableVertexAttribArray(0);
		
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		//render
		
			GL13.glActiveTexture(GL13.GL_TEXTURE0);
			hud.getTexture().bind();
			
			shaders.get("guiShader").updateUniform("transformationMatrix", hud.getTransformationMatrix());
			GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, hud.getModel().getVertexCount());
		
//		GL11.glEnable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL20.glDisableVertexAttribArray(0);
		GL30.glBindVertexArray(0);
	}
	
	public void renderHuds(Hud ... hud){
		if(!variables.containsKey("hud") || !variables.get("hud"))
			return;
		

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		shaders.get("guiShader").bind();
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		for(Hud h : hud){
			GL30.glBindVertexArray(h.getModel().getVaoID());
			GL20.glEnableVertexAttribArray(0);
			h.getTexture().bind();
			shaders.get("guiShader").updateUniform("transformationMatrix", h.getTransformationMatrix());
			GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, h.getModel().getVertexCount());
		}
		
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL20.glDisableVertexAttribArray(0);
		GL30.glBindVertexArray(0);
	}

	public void renderHudPro(HudPro hud) {
		shaders.get("guiShader").bind();
		GL30.glBindVertexArray(hud.getModel().getVaoID());
		GL20.glEnableVertexAttribArray(0);
		
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		//render
			shaders.get("guiShader").updateUniform("mousePosition", Input.getMousePosition());
			shaders.get("guiShader").updateUniform("resolution", parent.getWindow().getCanvasSize());
			shaders.get("guiShader").updateUniform("falloff", Config.MOUSE_ATTENUATION);
			shaders.get("guiShader").updateUniform("lightColor", Config.MOUSE_LIGHT_COLOR);
			shaders.get("guiShader").updateUniform("ambientColor", Config.MOUSE_AMBIENT_COLOR);
			
			
			GL13.glActiveTexture(GL13.GL_TEXTURE0);
			hud.getTexture().bind();
			
			GL13.glActiveTexture(GL13.GL_TEXTURE1);
			hud.getNormal().bind();
			
			shaders.get("guiShader").connectTextures();
			
			shaders.get("guiShader").updateUniform("transformationMatrix", hud.getTransformationMatrix());
			GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, hud.getModel().getVertexCount());
		
//		GL11.glEnable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL20.glDisableVertexAttribArray(0);
		GL30.glBindVertexArray(0);
	}
}
