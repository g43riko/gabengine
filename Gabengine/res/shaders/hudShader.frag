#version 130

in vec2 textureCoords;

out vec4 out_Color;

uniform sampler2D guiTexture;
uniform sampler2D normalSampler;
uniform vec3 falloff = vec3(.8, 6, 40);
uniform vec2 mousePosition;
uniform vec2 resolution = vec2(800, 600);
uniform vec4 lightColor = vec4(5, 5, 5, 1);
uniform vec4 ambientColor = vec4(.1, .1, .1, 1);

vec3 calcColor(vec2 mousePos, vec4 color){
	vec4 diffuseColor = texture(guiTexture, textureCoords);
	vec3 normalMap = texture2D(normalSampler, textureCoords).rgb;
	
	normalMap.r = 1 - normalMap.r;
	//normalMap.g = 1 - normalMap.g;
	
	vec3 lightDir = vec3(mousePos / resolution - (gl_FragCoord.xy / resolution), 0.5);
	lightDir.x *= resolution.x / resolution.y;
	
	float D = length(lightDir);
	
	vec3 N = normalize(normalMap * 2.0 - 1.0);
    vec3 L = normalize(lightDir);
    
    vec3 diffuse = (color.rgb * color.a) * max(dot(N, L), 0.0);

    vec3 ambient = ambientColor.rgb * ambientColor.a;
	
	float attenuation = 1.0 / ( falloff.x + (falloff.y*D) + (falloff.z*D*D) );
	
	vec3 Intensity = ambient + diffuse * attenuation;
    vec3 FinalColor = diffuseColor.rgb * Intensity;
    return FinalColor;
}

void main(void){
	vec4 DiffuseColor = texture(guiTexture, textureCoords);
	vec3 sum = vec3(0); 
	sum += calcColor(mousePosition, vec4(5,5,5,1));
	//sum += calcColor(resolution - mousePosition, vec4(0,0,5,1));
	
	
	//out_Color = texture(guiTexture,textureCoords);
	out_Color = vec4(sum, DiffuseColor.a);
	
}